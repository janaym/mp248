import numpy
def gauss(x, a=1, b=0, c=1):
    '''Returns x put through the gaussian equation
    
    Parameters
    ----------
    
    x : float, input value
    a : int, default value is 1
    b : int, default value is 0
    c : int, default value is 1
    
    '''
    return a*numpy.exp(-(x-b)**2 / ( 2*c**2))
