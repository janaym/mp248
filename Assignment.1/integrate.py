import functions as f
import math as m
import numpy as np

def sym_gauss_int_sqr(xb, n=10):
    ''' function that returns the integral of the standard Gaussian functio over the interval [-xb,xb] calculated with the area of 10 trapezoids.
    
    parameters:
    
    xb : float, defines the boundaries of the integral
    n  : int, gives the number of trapeizoids to be calculated.
    '''
    dx = 2*xb/n
    x_val = np.arange(-xb, xb+dx, dx)
    yl_val = np.arange( -xb, xb, dx)
    yr_val = np.arange(-xb+dx, xb+dx, dx)

    
    y_left = [f.gauss(i, c=m.sqrt(1/2)) for i in x_val]
    y_right = [f.gauss(i, c=m.sqrt(1/2)) for i in x_val]
    a_trap = [(y_left[i] + y_right[i])*dx/2 for i in range(n)]
    return sum(a_trap)
